"use strict";

const displayCoin = document.getElementById("displayCoin");
const displayText = document.getElementById("displayText");
const flipCoin = document.getElementById("flipCoin");

flipCoin.addEventListener("click", flipper);

/**
 * This function picks a nummber between 0 or 1
 * and displays the image that matches the number
 */
async function flipper() {
displayCoin.src = "flipping.gif";
    displayText.innerText = "Flipping...";
    await delay(1000);
    let num = parseInt(Math.random()*2);  //Randomly generating numbers 0 or 1
    let imgName;
    let imgAlt;
    let imgText;

    switch (num) {
        case 1:                            //If the random number is 1, the following values are assigned.
            imgName = "head2.jpg";
            imgAlt = "Heads";
            imgText = "HEADS!";
            break;
        
        case 0:                             //If the random number is 0, the following values are assigned.
            imgName = "tail2.jpg";
            imgAlt = "Tails";
            imgText = "TAILS!"
            break;
    }

    displayCoin.src = imgName;
    displayCoin.alt = imgAlt;
    displayText.innerText= imgText;
}

const delay = (ms) => new Promise((res) => setTimeout(res, ms));