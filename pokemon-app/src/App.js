import List from "./Components/List"
import './App.css';
import pokemonlogo from './Images/pokemonlogo.png';
import { makeStyles } from '@material-ui/core/styles';
import {React, useState} from 'react';
import PokemonView from './Components/PokemonView';
import PokemonAdd from './Components/PokemonAdd';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginBottom: '3%',
    },
    title: {
        flexGrow: 1,
        alignSelf: 'flex-end',
    },
    plogo: {
      maxWidth: '15%',
      justifySelf: 'center',
      borderRadius: '30px',
      marginBottom: '2%' 
    },
    wrapper: {
      marginTop: '3%'
    }
}));

function App() {
  const classes = useStyles();
  const [component, setComponent] = useState('list');
  const [currentId, setCurrentId] = useState([]);
  const [alertType, setAlertType] = useState("");

  return (
    <div className="App">
      <div className={classes.wrapper}>
      <img src={pokemonlogo} className={classes.plogo} alt="pokemonlogo"/>
      </div>
      {component === 'list' && <List setComponent={setComponent} 
        setCurrentId={setCurrentId} 
        setAlertType={setAlertType}
        alertType={alertType}/>}
      {component === 'view' && <PokemonView setComponent={setComponent} 
        currentId={currentId}
        setAlertType ={setAlertType}
        alertType={alertType}/>}
      {component === 'add' && <PokemonAdd setComponent={setComponent}
        setAlertType ={setAlertType}
        alertType={alertType}/>}
    </div>
  );
}

export default App;
