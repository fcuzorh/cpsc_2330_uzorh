import axios from "axios";
import {useEffect, useState} from 'react';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import MuiAlert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        maxWidth: '800px',
        maxHeight: '1000px',
        marginLeft:'auto',
        marginRight: 'auto',
        marginBottom: '20%',
        marginTop: '5%',
        justifyContent: 'center',
        width: '100%',
        '& > * + *': {marginTop: theme.spacing(2), },
    },
    details: {
        display: 'flex',
        flexDirection: 'row',
        textAlign: 'left',
    },
    content: {
        flex: 'auto auto',
    },
    cover: {
        width: '80%',
    },
    controls: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    },
    
    des: {
        maxWidth: '1000px',
        fontSize: '100%',
    }
}));


/**
 * PokemonView
 * @param {object} props
 * This function takes in an Id prop and displays the pokemon that has the Id 
 */
function PokemonView (props) {

    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [pokemon, setPokemon] = useState({});
    const [loaded, setLoaded] = useState(false);

    /**
     * Alert
     * @param {object} props
     * creates a new alert to be displayed 
     */
    function Alert(props) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
    }

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    }

    useEffect(() => {

        /**
         * getPokemon
         * @param {int} id
         * uses axios to pull down all the info of a pokemon based on their Id 
         */
        async function getPokemon(id) {
            try {
                const response = await axios({
                    method: 'get',
                    url: `https://web-app-pokemon.herokuapp.com/pokemon/${id}`,
                    headers: {
                    'User-Id': "FrankID123",
                    'Content-Type': "application/json"
                    }
                })
                //do something on success
                setPokemon(response.data);
                setLoaded(true);
                props.setAlertType("");
            } catch (e) {
                //some error occurred
                props.setAlertType("view-error");
                console.log(e);
                handleClick();
            }
        }
        getPokemon(props.currentId);
    }, []);

    return ( 
    <div>
        {loaded === true &&  
            <Card className={classes.root}>
                <div className={classes.details}>
                    <div className={classes.controls}>
                        <IconButton aria-label="back">
                            <ArrowBackIosIcon onClick={()=>{props.setComponent('list')}}/>
                        </IconButton>
                    </div>
                    <CardContent className={classes.content}>
                        <Typography component="h5" variant="h5">
                            {pokemon.name}
                        </Typography>
                        <Typography variant="subtitle1" color="textSecondary">
                            {pokemon.type}
                        </Typography>
                        <Typography variant="subtitle1" color="textSecondary" className={classes.des}>
                            {pokemon.description}
                        </Typography>
                    </CardContent>
                </div>
                <CardMedia className={classes.cover} image={pokemon.image} title={pokemon.name}/>
            </Card>
        }
        {props.alertType === "view-error" && <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="error">
                    Failed to retrieve pokemon
                </Alert>
            </Snackbar>}
    </div>
    );
}

export default PokemonView