import axios from "axios";
import React, {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import DeleteIcon from '@material-ui/icons/Delete';
import { grey } from "@material-ui/core/colors";
import VisibilityIcon from '@material-ui/icons/Visibility';
import Icon from '@material-ui/core/Icon';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import MuiAlert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';


const useStyles = makeStyles((theme) => ({
    root: {
        color: theme.palette.text.primary,
        flexGrow: 1,
        width: '100%',
        '& > * + *': {marginTop: theme.spacing(2), },
    },
    table: {
        minWidth: 650,
        
    },
    tableContainer: {
        marginLeft: 'auto',
        marginRight: 'auto',
        justifyContent: 'center',
        maxWidth: '70%',
        marginTop: '1%',
        paddingLeft: '1%',
        paddingRight: '1%',
    },

    title: {
        flexGrow: 1,
        alignSelf: 'flex-end',
    },
    plus: {
        fontSize: 80,
        color: theme.palette.secondary.main,
    },
    plusHolder: {
        marginTop: '2%',
        marginBottom: '15%',
    },
    buttons: {
        marginRight: '3%',
    }
}));


/**
 * List
 * @param {object} props
 * pulls down the list of pokemons and sends out the Id of a selected pokemon
 */
function List(props) {
    const classes = useStyles();
    const [pokemons, setPokemons] = useState([]);
    const [open, setOpen] = React.useState(false);
    const [deletedPokemon, setDeletedPokemon]= useState("");

    /**
     * Alert
     * @param {object} props
     * creates a new alert to be displayed 
     */
    function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
    }

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
        return;
    }

    setOpen(false);
    }

    useEffect(() => {    
        listPokemon();
        console.log(props);
        if (props.alertType){
            setOpen(true);
        }
    }, []);


    /**
     * listPokemon
     * uses axios to pull the list of pokemons from the webservice.
     */
    async function listPokemon() {
            try {
                const response = await axios({
                    method: 'get', //you can set what request you want to be: delete, get, post
                    url: 'https://web-app-pokemon.herokuapp.com/pokemon',
                    headers: {
                        "User-Id": "FrankID123",
                        "Content-Type": "application/json"
                    }
                });
                //do something on success
                setPokemons(response.data);
            } catch (e) {
                props.setAlertType("list-error");
                console.log(e);
                handleClick();
            }
        }
    

    /**
     * setViewId
     * @param {int} id
     * set the id of the pokemon to be viewed 
     */
    function setViewId(id) {
        props.setCurrentId(id);
        props.setComponent("view");
    }


    /**
     * deletePokemon
     * @param {int} id 
     * @param {string} name
     * takes in the id of the pokemon and uses it to delete the pokemon.
     * takes in the name of the pokemon and displays it in the alert. 
     */
    async function deletePokemon(id,name){
        try {
            const response = await axios({
            method: 'delete', //you can set what request you want to be: delete, get, post
            url: `https://web-app-pokemon.herokuapp.com/pokemon/${id}`,
            headers: {
                'User-Id': "FrankID123",
            }
        })
        //do something on success
        setDeletedPokemon(name);
        props.setAlertType("del-success");
        handleClick();
            
            listPokemon();
        } catch (e) {
            props.setAlertType("del-error");
            handleClick();
            console.log(e);
        }
    }

    return (
        <div>
            <TableContainer component={Paper} className={classes.tableContainer}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell align="center">Type</TableCell>
                            <TableCell align="right">Actions
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {pokemons.map((pokemon) => (
                        <TableRow key={pokemon.id}>
                            <TableCell component="th" scope="row">
                                {pokemon.name}
                            </TableCell>
                            <TableCell align="center">{pokemon.type}</TableCell>
                            <TableCell align="right">{
                                <Grid container className={classes.root}>
                                    <Grid item xs={12}>
                                        <IconButton className={classes.buttons}>
                                            <VisibilityIcon style={{ color: grey[400]}} onClick={()=>setViewId(pokemon.id)} />
                                        </IconButton>
                                        <IconButton className={classes.buttons}>
                                            <DeleteIcon style={{ color: grey[400] }} onClick={() =>deletePokemon(pokemon.id,pokemon.name)} />
                                        </IconButton>
                                    </Grid>
                                </Grid>}
                            </TableCell>
                        </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <div className={classes.root}>
                    <IconButton className={classes.plusHolder}>
                        <AddCircleIcon className={classes.plus} onClick={()=>props.setComponent('add')}/>
                    </IconButton>
            </div>
            {props.alertType === "del-success" && <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success">
                    {deletedPokemon} was deleted!
                </Alert>
            </Snackbar>}
            {props.alertType === "list-error" && <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="error">
                    Failed to retrieve pokemon list
                </Alert>
            </Snackbar>}
            {props.alertType === "del-error" && <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="error">
                    Failed to delete pokemon
                </Alert>
            </Snackbar>}
            {props.alertType === "add-success" && <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success">
                    Pokemon was added!
                </Alert>
            </Snackbar>}

        </div>
    );
}

export default List;