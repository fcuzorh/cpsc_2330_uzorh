import axios from "axios";
import React from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import {useState} from 'react';
import MuiAlert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';


const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(2),
            width: '30ch',
            padding: '1%',
            display: 'flex',
        },
        width: '100%',
            '& > * + *': {
                marginTop: theme.spacing(2),
    },
        marginBottom: '15%',
    },
    holder: {
        background: 'white',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginBottom: '12%',
        marginTop: '5%',
        justifyContent: 'center',
        maxWidth: '800px',
        padding: '2%',
        borderRadius: '10px',
        display: 'flex',
        flexDirection: 'row',
        textAlign: 'right',
    },

    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '25ch',
    },

    controls: {
        display: 'flex',
        alignItems: 'left',
        paddingLeft: theme.spacing(0),
        paddingRight: theme.spacing(3),
        paddingTop: theme.spacing(6),
        height: '60px',
    },
    
    icon: {
        fontSize: 40,
    },

    done: {
        display: 'flex',
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
        paddingLeft: theme.spacing(0),
        paddingRight: theme.spacing(0),
        paddingTop: theme.spacing(6.5),
        height: '60px',
    }
}));


/**
 * PokemonAdd
 * @param {object} props
 * this function collects information about a new pokemonn and posts it to the web service. 
 */
function PokemonAdd(props) {

    const classes = useStyles();
    const [name, setName] = useState("");
    const [type, setType] = useState("");
    const [description, setDescription] = useState("");
    const [image, setImage] = useState("false");

    const [open, setOpen] = useState(false);

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };


    /**
     * Alert
     * @param {object} props
     * creates a new alert to be displayed 
     */
    function Alert(props) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
    }


    
    async function addPokemon() {

        /**
         * addPokemon
         * uses axios to post the pokemon information to the webservice
         */
        if (name && type && description && image){
            try {
                const response = await axios({
                    method: 'post',
                    url: 'https://web-app-pokemon.herokuapp.com/pokemon',
                    data: {name: name, type: type, description: description, image: image},//only include data on post/put
                    headers: {
                        'User-Id': "FrankID123",
                        'Content-Type': "application/json"
                    }
                })
             //do something on success
                console.log(response);
                props.setAlertType("add-success");
                props.setComponent('list');
            } catch (e) {
             //some error occurred
                handleClick();
                console.log(e);
                props.setAlertType("add-error");
            }
        }
}
    return (
    <form className={classes.root} noValidate autoComplete="off">       
        <div className={classes.holder}>
            <div className={classes.controls}>
                <IconButton aria-label="back" >
                    <ArrowBackIosIcon className={classes.icon} onClick={()=>{props.setComponent('list')}}/>
                </IconButton>
            </div>
            <div>
            <TextField
                id="outlined-full-width"
                label=" Name"
                style={{ margin: 8 }}
                fullWidth
                margin="normal"
                InputLabelProps={{
                    shrink: true,
                }}
                variant="outlined"
                onChange={(event) => setName(event.target.value)}
            />

            <TextField
                id="outlined-full-width"
                label=" Type"
                style={{ margin: 8 }}
                fullWidth
                margin="normal"
                InputLabelProps={{
                    shrink: true,
                }}
                variant="outlined"
                onChange={(event) => setType(event.target.value)}
            />
            </div>

            <div>
            <TextField
                id="outlined-full-width"
                label=" Description"
                style={{ margin: 8 }}
                fullWidth
                margin="normal"
                InputLabelProps={{
                    shrink: true,
                }}
                variant="outlined"
                className="classes.wide"
                onChange={(event) => setDescription(event.target.value)}
            />
            
            <TextField
                id="outlined-full-width"
                label=" Image Link"
                style={{ margin: 8 }}
                fullWidth
                margin="normal"
                InputLabelProps={{
                    shrink: true,
                }}
                variant="outlined"
                onChange={(event) => setImage(event.target.value)}
            />
            </div>
            <div className={classes.done}>
                <IconButton aria-label="done" >
                    <CheckCircleIcon className={classes.icon} onClick={addPokemon}/>
                </IconButton>
            </div>
            {props.alertType === "add-error" && <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="error">
                    Failed to add pokemon
                </Alert>
            </Snackbar>}
        </div>
    </form>
    );
}

export default PokemonAdd;