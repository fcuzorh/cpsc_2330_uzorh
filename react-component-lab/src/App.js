import './App.css';
import MadLib from "./Components/MadLib";
import MovieList from "./Components/MovieList";
import React, { useState } from "react";
import train from "./Images/train.png";

function App() {
  const [showMadLib, setShowMadLib] = useState(false);
  const [showMovieList, setShowMovieList] = useState(false);

  return (
    <div className="App">
      <h1 className="Banner">FUN STATION</h1>
      
      <input className="CheckBox" type="checkbox" checked={showMadLib} onChange={(event) => setShowMadLib(event.target.checked)}></input>
      <label className="CheckLab">MADLIB</label>
      
      <input className="CheckBox" type="checkbox" checked={showMovieList} onChange={(event) => setShowMovieList(event.target.checked)}></input>
      <label className="CheckLab">MOVIE-LIST</label>
    
    {showMadLib && ( <React.Fragment> <MadLib /> </React.Fragment> )}
    {showMovieList && ( <React.Fragment> <MovieList /> </React.Fragment> )}

    <img src={train} className="Image" alt="Train"></img>

    </div>
  );
  
}

export default App;
