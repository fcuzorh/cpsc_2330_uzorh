
const movies = [
    {title: "Spider Man", year: 2020},
    {title: "Wonder Woman", year: 2019},
    {title: "Super Man", year: 2017}
];

/**
 * This fuction displays all the movie in the array on a table.
 */

function MovieList() {
    return (
        <table className="Table">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Year</th>
                </tr>
            </thead>
            <tbody>
                {movies.map((movie) => {
                    return (
                        <tr>
                            <td>{movie.title}</td>
                            <td>{movie.year}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default MovieList;