import React, { useState } from "react";


/**
 * This fucntion allows user to update a story with the use of 3 textboxes
 * Each textbox has a useState for live update
 */
function MadLib() {
    const [person, setPerson] = useState("...");
    const [thing, setThing] = useState("...");
    const [adjective, setAdj] = useState("...");

    return(
        <div>
            <input placeholder="Person" className="Input" onChange={(event) => setPerson(event.target.value)}></input>
            <input placeholder="Something" className="Input" onChange={(event) => setThing(event.target.value)}></input>
            <input placeholder="Adjective" className="Input" onChange={(event) => setAdj(event.target.value)}></input>

            <div className="Story">{person} went to the market to buy a {thing},
            there wasn't any {thing} that he thought was {adjective} at the market,
            so {person} went to his neighbor who had a {thing} that was very {adjective}.</div>
        </div>
    )
}

export default MadLib;