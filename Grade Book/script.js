"use strict";

const tableBody = document.getElementById("tableBody");
const assignName = document.getElementById("assignName");
const pointsEarned = document.getElementById("pointsEarned");
const total = document.getElementById("total");
const addButton = document.getElementById("addButton");

const homePage = document.getElementById("homePage");
const gradePage = document.getElementById("gradePage");

const percent = document.getElementById("percent");
const letter = document.getElementById("letter");



// registering the click event to the add button - calling the addRecord function
addButton.addEventListener("click", addRecord);
addButton.addEventListener("click", calcGrade);

// Doing input validation in text box - calling the validateInputs function
assignName.addEventListener("input", validateInputs);
pointsEarned.addEventListener("input", validateInputs);
total.addEventListener("input", validateInputs);

// Updating the percentage and the letter grade any time there is a new grade

let gradeArray = [];
let idNum = 0;
let currentPage = "";

/**
 * Verifies that inputs both have values
 * If so, enables (removes the disabled attr) the add button,
 * otherwise, it disables the button
 */
function validateInputs() {
    if (assignName.value && pointsEarned.value && total.value) {
        addButton.removeAttribute("disabled");
        addButton.innerHTML="+";
    } else {
        addButton.setAttribute("disabled", "");
        addButton.innerText=" . ";
    }
}

/**
 * appends record to the array, then calls the renderTableBody function
 * checks to make sure the bonus points are intentional
 */
function addRecord() {
    
    if (parseInt(pointsEarned.value)>parseInt(total.value)){
        let check = confirm("Did this student get BONUS points, if so click OK")
        if (!check){
            return;
        }
    }

    gradeArray.push({ name: assignName.value, points: parseInt(pointsEarned.value), total: parseInt(total.value), id: idNum });
    idNum++;
    setData(gradeArray);
    renderTableBody();
    assignName.value = "";
    pointsEarned.value = "";
    total.value = ""
    validateInputs();
}

/**
 * Adds a record to the body of the table
 * REQUIRES assignment name, points, and total inputs to have a value
 * Clears out inputs after success
 */
function renderTableBody() {
    //clear out the table before we add all of the records to it
    tableBody.innerHTML = "";

    gradeArray.map((grade) => {
        const tableRow = tableBody.insertRow(-1);
        const nameCell = tableRow.insertCell(0);
        const pointsCell = tableRow.insertCell(1);
        const totalCell = tableRow.insertCell(2);
        const actionCell = tableRow.insertCell(3);

        nameCell.innerText = grade.name;
        pointsCell.innerText = grade.points;
        totalCell.innerText = grade.total;
        actionCell.innerHTML = `<button onclick="del(${grade.id})" class="actionClass"><i class="fas fa-trash-alt"></i></button>`;
    });
}

/**
 * filters the array by id, and removes the selected record
 * @param {autoincrementing integer} id
 */
function del(id) {
    //confirm the delete action first
    let res = confirm("Are you sure?");

    if (res === true){
        //retrieve all record but the selected record - returning an array
        gradeArray = gradeArray.filter((grade) => grade.id !== id);
        setData(gradeArray);
        renderTableBody();
        calcGrade();
    }
}

/**
 * applies display: none
 * makes the div disappear like multiple pages
 */
function homeAppear() {
    homePage.classList.add("visible");
    gradePage.classList.add("invisible");
    homePage.classList.remove("invisible");
    gradePage.classList.remove("visible");
    currentPage = "Home";
    setPage(currentPage);

}

/**
 * applies display: block
 * makes the div appear like multiple pages
 */
function gradeAppear() {
    homePage.classList.add("invisible");
    gradePage.classList.add("visible");
    homePage.classList.remove("visible");
    gradePage.classList.remove("invisible");
    currentPage = "Grade";
    setPage(currentPage);
}


/**
 * Calculates the grade percentage and letter grade of a student
 */
function calcGrade() {
    let num = 0;
    let tot = 0;
    let p = 0;
    let l = "";

    gradeArray.map((grade) => {
    num = num + grade.points;
    tot = tot + grade.total;
    p = Math.round((num/tot)*100);
})

    switch(true){
        case p >= 94:
            l = "A";
            break;
        
        case p < 94 && p >= 92:
            l = "A-";
            break;
        
        case p < 92 && p >= 89:
            l = "B+";
            break;
        
        case p < 89 && p >= 86:
            l = "B";
            break;

        case p < 86 && p >= 84:
            l = "B-";
            break;

        case p < 84 && p >= 81:
            l = "C+";
            break;

        case p < 81 && p >= 78:
            l = "C";
            break;

        case p < 78 && p >= 76:
            l = "C-";
            break;

        case p < 76 && p >= 65:
            l = "D";
            break;

        case p <= 64:
            l = "F";
            break;
    }
    percent.innerText=`${p}%`;
    letter.innerText=l;
}

/**
 * This function pulled data from localStorage when called
 */
function getData () {
    let storedData = window.localStorage.getItem("data");
    if (storedData){
    try {
        gradeArray = JSON.parse(storedData);

        idNum =
                Math.max.apply(
                    Math,
                    gradeArray.map((myId) => myId.id)
                ) + 1;
        renderTableBody();
    } catch (error) {
        console.log(error);
        gradeArray = [];
    }
}
}


/**
 * This function takes in a array, changes it to string and saves it to localStorage
 * @param {array} gradeRecords 
 */
function setData (gradeRecords) {
    try {
        window.localStorage.setItem("data", JSON.stringify(gradeRecords));
    } catch (error) {
        console.log(error);
    }
}

/**
 * This function gets the saved page that was active before the refresh and rerenders it.
 */
function getPage() {
    let storedPage = window.sessionStorage.getItem("page");
    if (storedPage){
        try {
            currentPage = storedPage;
            if (storedPage === "Grade"){
                gradeAppear();
            }
            else {
                homeAppear();
            }
        } catch (error) {
            console.log(error);
        }
    }

}

/**
 * This function sets the current page before refresh so that it can be rerendered.
 * @param {string} pageName 
 */
function setPage(pageName) {
    try {
        window.sessionStorage.setItem("page",pageName);
    } catch (error) {
        console.log(error);
    }
}

//Run getData on page load.
getData();
calcGrade();
getPage();