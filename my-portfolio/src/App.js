import './App.css';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Popover from '@material-ui/core/Popover';
import Button from '@material-ui/core/Button';
import biography from './images/biography.png';
import coinflip from './images/coinflip.png';
import gradebook from './images/gradebook.png';
import pokemon from './images/pokemon.png';
import searcher from './images/searcher.png';
import palindrome from './images/palindrome.png';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  root2: {
    maxWidth: '60%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(20),
    fontWeight: theme.typography.fontWeightLight,
    color: theme.palette.primary.main,
  },
  wall: {
    backgroundColor: theme.palette.tetiary.main,
    opacity: '85%',
  },
  image: {
    width: 200,
    height: 100,
    margin: '1%',
    border: 'solid black 1px',
    borderRadius: '5px',
  },
  holder: {
    margin: 'auto',
    justifyContent: 'center',
    width:'100%',
  },
  typography: {
    padding: theme.spacing(2),
  },
  title: {
    fontSize: 14,
  },
  info: {
    width: '80%',
    padding: '7%',
    paddingBottom: '2%',
    marginLeft: '5%',
    opacity: '80%',
    color: theme.palette.primary.main,
  },
  Banner: {
    color: 'white',
    textAlign: 'center',
    fontFamily: 'sans-serif',
    fontSize: '8vw',
    fontWeight: 'Bolder',
    marginTop: '0%',
    marginBottom: '0%',
},

  BannerHolder: {
  marginTop: '12vw',
  backgroundColor: 'black',
  opacity: '30%',
  maxWidth: '60%',
  marginRight: 'auto',
  marginLeft: 'auto',
  borderRadius: '5px',
  justifyContent: 'center',
  padding: '2%',
}

}));


function App() {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [detail, setDetail] = React.useState({});

  const biographyInfo = {
    Name: 'Biography Page',
    Description: 'This is a page that information about me and ways for people to contact me',
    More: 'It is a simple page with basic html elements and images',
    Link: 'https://frank-biography.netlify.app/',
    Code: 'https://bitbucket.org/fcuzorh/cpsc_2330_uzorh/src/master/Biography/'
  }

  const gradebookInfo = {
    Name: 'Gradebook App',
    Description: 'This is an app that allows user to track their grades, and see their average.',
    More: 'It uses display block and none to toggle between pages which are just divs',
    Link: 'https://frankgradebook.netlify.app/',
    Code: 'https://bitbucket.org/fcuzorh/cpsc_2330_uzorh/src/master/Grade%20Book/'
  }

  const coinflipInfo = {
    Name: 'Coinflip App',
    Description: 'This is an app that allows user to flip a coin and get heads or tails.',
    More: 'It uses a randomizer that randomly generates a 1 or 0 for head or tail',
    Link: 'https://frank-coinflip.netlify.app/',
    Code: 'https://bitbucket.org/fcuzorh/cpsc_2330_uzorh/src/master/Coin%20Flip/'
  }

  const searcherInfo = {
    Name: 'Search App',
    Description: 'This is an app that allows user to search through a list of sports',
    More: 'It is a react app that uses props and useState to update search keyword',
    Link: 'https://franks-react-props-lab.netlify.app/',
    Code: 'https://bitbucket.org/fcuzorh/cpsc_2330_uzorh/src/master/react-props-lab/'
  }

  const pokemonInfo = {
    Name: 'Pokemon App',
    Description: 'This is an app that allows user to track their pokemons.',
    More: 'It is a react app that uses components to view, add, and delete pokemons.',
    Link: 'https://frank-pokemon.netlify.app',
    Code: 'https://bitbucket.org/fcuzorh/cpsc_2330_uzorh/src/master/pokemon-app/'
  }

  const palindromeInfo = {
    Name: 'Palindrome App',
    Description: 'This is an app that allows user to check if a word is a palindrome.',
    More: 'Palindromes are words that have the same spelling when spelt backwards.',
    Link: 'https://frank-palindrome.netlify.app',
    Code: 'https://bitbucket.org/fcuzorh/cpsc_2330_uzorh/src/master/react-palindrome/'
  }

  const handleClickBio = (event) => {
    setDetail(biographyInfo);
    setAnchorEl(event.currentTarget);
  };

  const handleClickGrade = (event) => {
    setDetail(gradebookInfo);
    setAnchorEl(event.currentTarget);
  };

  const handleClickCoin = (event) => {
    setDetail(coinflipInfo);
    setAnchorEl(event.currentTarget);
  };

  const handleClickSearch = (event) => {
    setDetail(searcherInfo);
    setAnchorEl(event.currentTarget);
  };

  const handleClickPoke = (event) => {
    setDetail(pokemonInfo);
    setAnchorEl(event.currentTarget);
  };

  const handleClickPal = (event) => {
    setDetail(palindromeInfo);
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;


  return (
    <div className="App">
      <div className={classes.root}>
      <Accordion className={classes.wall}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography className={classes.heading}>Projects</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <span className={classes.holder}>
            <img src={biography} alt="biopage" className={classes.image} onClick={handleClickBio}></img>
            <img src={coinflip} alt="coinflippage" className={classes.image} onClick={handleClickCoin}></img>
            <img src={searcher} alt="searcherpage" className={classes.image} onClick={handleClickSearch}></img>
            <img src={gradebook} alt="gradebookpage" className={classes.image} onClick={handleClickGrade}></img>
            <img src={pokemon} alt="pokemonpage" className={classes.image} onClick={handleClickPoke}></img>
            <img src={palindrome} alt="palindromepage" className={classes.image} onClick={handleClickPal}></img>
          </span>
        </AccordionDetails>
      </Accordion>
      <Popover id={id} open={open} anchorEl={anchorEl} onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <div className={classes.info}>
          <Typography variant="h4" component="h2" style={{marginBottom: '3%'}}>
            {detail.Name}
          </Typography>
          <Typography variant="body2" component="p" style={{marginBottom: '5%'}}>
            {detail.Description}
          </Typography>
          <Typography variant="body2" component="p" style={{marginBottom: '5%'}}>
            {detail.More}
          </Typography>
          <Button size="small" style={{marginLeft: '25%', background: 'lightgrey'}} href={detail.Link} target='_blank'>Link</Button>
          <Button size="small" style={{marginLeft: '5%', background: 'lightgrey'}} href={detail.Code} target='_blank'>Code</Button>
          </div>
      </Popover>
      </div>
      <div className={classes.BannerHolder}>
        <h1 className={classes.Banner}>Francis Uzorh's</h1>
        <h1 className={classes.Banner}>Portfolio</h1>
      </div>
    </div>
  );
}

export default App;

//By Frank