"use strict";


// node uses an import system called CommmonJs
// CommonJs uses the keyword "require" to import files

const express = require("express");
const cors = require("cors");

//Make sure the modules are being used
app.use(cors());
app.use(bodyParser.json());

//Set a port
const PORT = 5000;

const bodyParser = require("body-parser");
const { v4: uuidv4 } = require("uuid");


//Instantiate express.
const app = express();



let videos = [
    {title: "Space Jam", relyear: "1995", avail: true, id: uuidv4()},
    {title: "Star Wars", relyear: "1997", avail: true, id: uuidv4()},
    {title: "Jurassic Park", relyear: "1993", avail: true, id: uuidv4()}
];

//http://localhost:5000/health
/**
 * two parameters: url & fucntion that gets called when handler function is hit.
 * funxtion handler has two parameters: 
 * the request(your api call) and 
 * the response (whet gets sent back to you)
 */
app.get("/health", (req, res) => {
    const resObject = {
        name: "Victor's Video Service",
        Version: "1.0.0"
    };

    //res.send(JSON.stringify(resObject));
    res.status(200);
    res.json(resObject) //if the recieveing platform can read JSON
})


//Get all vidoes
app.get("/videos", (req,res) => {
    res.status(200);
    res.json(videos);
})

//Get video by ID
app.get("/videos/:id",(req,res) => {
    const videoId = req.params.id;

    // first way
    // const filteredVideos = videos.filter((video) => video.id === vidoeId );
    // if (filteredVideos.length > 0) {
    //     res.json(filteredVidoes[0]);
    // }

    //other way
    const foundVideo = videos.find((video) => video.id === videoId);
    if (foundVideo){
        res.status(200);
        res.json(foundVideo);
    } else {
        res.status(404);
        res.json({message: `Video with id of '${videoId}' not found `});
    }
})


//Add an item (post)
app.post("/videos", (req,res) => {
    const title = req.body.title;
    const relyear = req.body.relyear;

    if (title && relyear){
        const newVideo = {
            id: uuidv4(),
            title: title,
            relyear: relyear,
            avail: true
        };

        videos.push(newVideo);
        res.status(200);
        res.json(newVideo);
    } else {
        res.status(400);
        res.json({message: "title and relyear are required fields in the body"});
    }
});

//delete an item
app.delete("/videos/:id", (req,res) => {
    const videoId = req.params.id;

    //return index of the first item that matches the id
    //returns -1 if not found
    const foundIndex = videos.findIndex((video) => video.id === videoId);

    //not using truthy here
    if (foundIndex > -1) {
        videos.splice(foundIndex, 1);
        res.status(200);
        res.json({message: "Item succesfully deleted"});
    } else {
        res.status(404);
        res.json({message: "Item not found"});
    }
});

//modifying a record
app.put("/videos/:id", (req, res) => {
    const videoId = req.params.id;
    //look for parameters in the request body
    const title = req.body.title;
    const relyear = req.body.relyear;

    if (title && relyear){
        const foundIndex = videos.findIndex((video) => video.id === videoId);

        if (foundIndex > -1){
            videos[foundIndex].title = title;
            videos[foundIndex].relyear = relyear;
            res.status(200);
            res.json(videos[foundIndex]);
        } else {
            res.status(404);
            res.json({message: "video not found"});
        }
    } else {
        res.status(400);
        res.json({message: "title and relyear are required parameters in the body"});
    }
});

//Checkout
app.post("/videos/:id/checkout", (req,res) => {
    const videoId = req.params.id;
    const foundIndex = videos.findIndex((video) => video.id === videoId);
    if (foundIndex > -1){
        if(videos[foundIndex].avail){
            videos[foundIndex].avail = false;
            res.status(200);
            res.json({message: "Video checked out successfully"});
        } else {
            res.status(409);
            res.json({message: `Video ${videoId} is already checked out`});
        }

    } else {
        res.status(404);
        res.json({message: "video not found"});
    }
})

//Checkin
app.post("/videos/:id/checkin", (req,res) => {
    const videoId = req.params.id;
    const foundIndex = videos.findIndex((video) => video.id === videoId);
    if (foundIndex > -1){
        videos[foundIndex].avail = true;
        res.status(200);
        res.json({message: "Video checked in successfully"});
    } else {
        res.status(404);
        res.json({message: "video not found"});
    }
})

//Tell application to start up and listen to specified port
//takes a second arugument of a fucntion to run when its started.
app.listen(PORT, () => console.log("welcome to Victor's Video Venue"));
