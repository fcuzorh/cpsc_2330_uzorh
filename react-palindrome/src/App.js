import './App.css';
import React, { useState } from "react";

function App() {
  
  const [word, setWord] = useState("");
  const [status, setStatus] = useState("");
  
  /**
   * function that check if the input text is a palindrome
   * splits into an array, reverses it, and the compares it with original text
   */
  function checkWord() {
    const checker = word.replace(" ","").toUpperCase();
    const reverse = checker.split("").reverse().join("");
    if (reverse === checker){
      setStatus("Yay! It is a Palindrome :)");
    } else {
      setStatus("Oops! It is not a Palindrome :/");
    }
  }
  
  return (
    <div className="App">
      <div>
        <h1 className="Banner">Palindrome Checker</h1>
      </div>
      <span>
        <input className="textBox" type="text" placeholder="Type your word ;)" onChange={(event) => setWord(event.target.value)}></input>
        <button className="button" onClick={checkWord}>Check!</button>
      </span>
      <div>
        <h2 className="Result">{status}</h2>
      </div>
    </div>
  );
}

export default App;
