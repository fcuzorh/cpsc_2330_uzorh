/**
 * This fuction takes in text and set it as a search term
 * @param {object} props 
 * setSearchTerm - function.
 */
function Search(props) {
    return(
        <input className="search" placeholder="Enter Text" onChange={(event) => props.setSearchTerm(event.target.value)}></input>
    )
}

export default Search;