const sports = ["Basketball", 
                "Volleyball", 
                "Soccer", 
                "Football", 
                "Baseball", 
                "Softball", 
                "Wrestling", 
                "Golf"];

/**
 * This function displays a list of sports based on a search term.
 * @param {object} props 
 * searchTerm - string.
 */
function List(props){
    return (
        <table className="Table">
            <thead>
                <tr>
                    <th>Sports</th>
                </tr>
            </thead>
            <tbody>
                {sports.filter(sport => sport.toUpperCase().includes(props.searchTerm.toUpperCase())).map((sport) => {
                    return (
                        <tr>
                            <td>{sport}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default List;