import React, { useState } from "react";
import './App.css';
import Search from "./Components/Search";
import List from "./Components/List";


function App() {
  const [searchTerm, setSearchTerm] = useState("");

  return (
    <div className="App">
      <div className="Banner">
        <h1>Sport Searcher</h1>
        <div className="searchBar"><Search setSearchTerm = {setSearchTerm}/></div>
        <div className="list"><List searchTerm={searchTerm}/></div>
      </div> 
    </div>
  );
}

export default App;
